﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace hostel
{
    public partial class OplataStud : Form
    {
        public Form1 f1;
        public int selectrow = -1;
        public OplataStud()
        {
            InitializeComponent();
        }

        private void OplataStud_Load(object sender, EventArgs e)
        {
            int y = f1.X;
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                      "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;

            string sql = "SELECT Оплата.id, Студенты.ФИО, Оплата.Дата, Оплата.Сумма " +
                "FROM Студенты INNER JOIN Оплата ON Студенты.[id] = Оплата.[id_студента] WHERE Студенты.id = " + y ;

            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();

            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Оплата");
            dataGridView1.DataSource = ds.Tables["Оплата"].DefaultView;
            connection.Close();

            connection.ConnectionString = ConnectionString;

            sql = "Select * FROM Студенты";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();

            da = new OleDbDataAdapter(myCommand);
            ds = new DataSet();
            da.Fill(ds, "Студ");
            dataGridView2.DataSource = ds.Tables["Студ"].DefaultView;
            connection.Close();

            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = false;

        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = -1;
           
            dateTimePicker1.Text = "";
            textBox3.Text = "";
            groupBox1.Visible = true;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
        }

        private void редактироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {

            textBox4.Text = "";
            dateTimePicker2.Text = "";
           
            textBox7.Text = "";

            groupBox1.Visible = false;

            groupBox2.Visible = true;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
        
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox8.Text = "";


            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = true;
            groupBox4.Visible = false;
        
        }

        private void поискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = -1;
            textBox9.Text = "";

            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = true;
        
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox4.Text = "";
             dateTimePicker2.Text = "";
            textBox7.Text = "";

            textBox8.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;
            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox4.Text = dataGridView1[0, selectrow].Value.ToString();
                dateTimePicker2.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox7.Text = dataGridView1[3, selectrow].Value.ToString();

                textBox8.Text = dataGridView1[0, selectrow].Value.ToString();

            }


        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox4.Text = "";
            dateTimePicker2.Text = "";
           textBox7.Text = "";

            textBox8.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;

            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox4.Text = dataGridView1[0, selectrow].Value.ToString();
                dateTimePicker2.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox7.Text = dataGridView1[3, selectrow].Value.ToString();

                textBox8.Text = dataGridView1[0, selectrow].Value.ToString();

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int y = f1.X;
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Необходимо выбрать строку для удаления");
                return;
            }

            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                        "DatA Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;

            sql = " Delete * FROM Оплата where id = " + textBox8.Text.ToString();
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT Оплата.id, Студенты.ФИО, Оплата.Дата, Оплата.Сумма " +
                "FROM Студенты INNER JOIN Оплата ON Студенты.[id] = Оплата.[id_студента] WHERE Студенты.id = " + y;

            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox3.Visible = false;

        }

        private void button6_Click(object sender, EventArgs e)
        {
            groupBox3.Visible = false;
        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int y = f1.X;
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Необходимо выбрать строку для редактирования");
                return;
            }

            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                        "DatA Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;

            sql = " UPDATE Студенты INNER JOIN Оплата ON Студенты.[id] = Оплата.[id_студента] SET " +
                "Оплата.id_студента = " + y + "," +
                "Оплата.Дата = '" + dateTimePicker2.Value.ToString("dd.MM.yyyy") + "'," +
                "Оплата.Сумма = " + textBox7.Text.ToString() + "" +
               " WHERE Оплата.id =" + textBox4.Text.ToString();
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT Оплата.id, Студенты.ФИО, Оплата.Дата, Оплата.Сумма " +
                 "FROM Студенты INNER JOIN Оплата ON Студенты.[id] = Оплата.[id_студента] WHERE Студенты.id = " + y;
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox1.Visible = false;
            groupBox4.Visible = false;
            groupBox3.Visible = false;
            groupBox2.Visible = false;
            }
            catch
            {
                MessageBox.Show("Ошибка");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int y = f1.X;
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                                  "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;

            string sql;

            sql = "INSERT INTO Оплата ( id_студента, Дата, Сумма )" +
    "VALUES(" + y + ",' " + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "', '" + textBox3.Text + "')";

            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT Оплата.id, Студенты.ФИО, Оплата.Дата, Оплата.Сумма " +
                 "FROM Студенты INNER JOIN Оплата ON Студенты.[id] = Оплата.[id_студента] WHERE Студенты.id = " + y;
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox1.Visible = false;
            groupBox4.Visible = false;
            groupBox3.Visible = false;
            groupBox2.Visible = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {

            groupBox1.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int y = f1.X;
            if (comboBox1.SelectedIndex < 0)
            {
                MessageBox.Show("Введите критерий поиска");
                return;
            }
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " +
                       "Data Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            string kriteriy = comboBox1.SelectedItem.ToString();
            string poisk = textBox9.Text;
            string sql = "SELECT Оплата.id, Студенты.ФИО, Оплата.Дата, Оплата.Сумма " +
                            "FROM Студенты INNER JOIN Оплата ON Студенты.[id] = Оплата.[id_студента]"+
                             " WHERE Студенты.id = " + y;
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Дата":
                    sql += "AND Оплата.Дата" + " like '" + poisk + "%' ";
                    break;
                case "Сумма":
                    sql += "AND Оплата.Сумма" + " like '" + poisk + "%'";
                    break;
             }

            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

        }

        private void button8_Click(object sender, EventArgs e)
        {
            int y = f1.X; 
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                      "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;


            string sql = "SELECT Оплата.id, Студенты.ФИО, Оплата.Дата, Оплата.Сумма " +
                "FROM Студенты INNER JOIN Оплата ON Студенты.[id] = Оплата.[id_студента] WHERE Студенты.id = " + y;

            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
            groupBox4.Visible = false;
        
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

     }
}
