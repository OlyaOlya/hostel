﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace hostel
{
    public partial class KlasR : Form
    {
        public int selectrow = -1;

        public KlasR()
        {
            InitializeComponent();
        }

        private void KlasR_Load(object sender, EventArgs e)
        {
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                       "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;

            string sql = "Select * FROM Класн_рук";
            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();

            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "ПІБ");
            dataGridView1.DataSource = ds.Tables["ПІБ"].DefaultView;

            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                       "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;

            string sql ;

           sql = "INSERT INTO Класн_рук (ФИО, группа, телефон) " +
                "VALUES ('" + textBox1.Text + "','"  + textBox2.Text + "', '"  + textBox3.Text +
                               "')";

            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "Select * FROM Класн_рук";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox1.Visible = false;
            groupBox4.Visible = false;
            groupBox3.Visible = false;
            groupBox2.Visible = false;

        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            groupBox1.Visible = true;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = false;


        }

        private void button2_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
        }

        private void редактироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";

            groupBox1.Visible = false;

            groupBox2.Visible = true;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox4.Text = "";
            textBox5.Text = "";
            textBox5.Text = "";
            textBox7.Text = "";

            textBox8.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;

            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox4.Text = dataGridView1[0, selectrow].Value.ToString();
                textBox5.Text = dataGridView1[1, selectrow].Value.ToString();
                textBox6.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox7.Text = dataGridView1[3, selectrow].Value.ToString();

                textBox8.Text = dataGridView1[0, selectrow].Value.ToString();
            }


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";

            textBox8.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;
            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox4.Text = dataGridView1[0, selectrow].Value.ToString();
                textBox5.Text = dataGridView1[1, selectrow].Value.ToString();
                textBox6.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox7.Text = dataGridView1[3, selectrow].Value.ToString();

                textBox8.Text = dataGridView1[0, selectrow].Value.ToString();
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Необходимо выбрать строку для редактирования");
                return;
            }

            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                        "DatA Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;

            sql = " UPDATE Класн_рук SET " +
              //  "id = " + textBox4.Text.ToString() + "," +
                "ФИО = '" + textBox5.Text.ToString() + "'," +
                "Группа = '" + textBox6.Text.ToString() + "'," +
                "Телефон = '" + textBox7.Text.ToString() + "'" +
               " WHERE id =" + textBox4.Text.ToString();
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "Select * FROM Класн_рук";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox2.Visible = false;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Необходимо выбрать строку для удаления");
                return;
            }

            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                        "DatA Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;

            sql = " Delete * FROM Класн_рук where id = " + textBox8.Text.ToString();
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "Select * FROM Класн_рук";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox3.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            groupBox3.Visible = false;
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox8.Text = "";

            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = true;
            groupBox4.Visible = false;
        }

       
        private void поискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = -1;
            textBox9.Text = "";

            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
           if (comboBox1.SelectedIndex < 0)
            {
                MessageBox.Show("Введите критерий поиска");
                return;
            }
                        string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " +
                                   "Data Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            string kriteriy = comboBox1.SelectedItem.ToString();
            string poisk = textBox9.Text;
            string sql = "SELECT * FROM Класн_рук WHERE " +
                        kriteriy + " like '" + poisk + "%' ";

            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

        }

        private void button8_Click(object sender, EventArgs e)
        {
            
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                       "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;

          
            string sql = "Select * FROM Класн_рук";
            
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
            groupBox4.Visible = false;
        }

        

        

               
    }
}
