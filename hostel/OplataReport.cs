﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace hostel
{
    public partial class OplataReport : Form
    {
        public OplataReport()
        {
            InitializeComponent();
        }

        private void OplataReport_Load(object sender, EventArgs e)
        {
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                      "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;

            string sql = "SELECT Оплата.id, Студенты.ФИО, Оплата.Дата, Оплата.Сумма " +
                "FROM Студенты INNER JOIN Оплата ON Студенты.[id] = Оплата.[id_студента]";
            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();

            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Оплата1");
            ds.WriteXmlSchema("schema2.xml");
            connection.Close();

            CrystalReport1 rpt1 = new CrystalReport1();
            rpt1.SetDataSource(ds);
            crystalReportViewer1.ReportSource = rpt1;
        }
    }
}
