﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace hostel
{
    public partial class Parrent : Form
    {
        public int selectrow = -1;

        public Parrent()
        {
            InitializeComponent();
        }

        private void Parrent_Load(object sender, EventArgs e)
        {
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                      "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;

            string sql = "Select * FROM Родители";
            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();

            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "ФИО");
            dataGridView1.DataSource = ds.Tables["ФИО"].DefaultView;

            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void редактироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";

            groupBox2.Visible = true;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
            groupBox1.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Необходимо выбрать строку для редактирования");
                return;
            }

            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                      "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;

            sql = "UPDATE Родители SET " +
                //  "id = " + textBox4.Text.ToString() + "," +
                "ФИО_матери = '" + textBox5.Text.ToString() + "'," +
                "Место_работы_м = '" + textBox6.Text.ToString() + "'," +
                "Телефон_м = '" + textBox7.Text.ToString() + "'," +
                "ФИО_отца = '" + textBox1.Text.ToString() + "'," +
                "Место_работы_от = '" + textBox2.Text.ToString() + "'," +
                "Телефон_от = '" + textBox3.Text.ToString() + "' " +
               " WHERE id =" + textBox4.Text.ToString();
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "Select * FROM Родители";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "ФИО");
            dataGridView1.DataSource = ds.Tables["ФИО"].DefaultView;
            connection.Close();
            groupBox2.Visible = false;

        }


     
                  private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox5.Text = "";
            textBox7.Text = "";

            textBox8.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;

            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox1.Text = dataGridView1[4, selectrow].Value.ToString();
                textBox2.Text = dataGridView1[5, selectrow].Value.ToString();
                textBox3.Text = dataGridView1[6, selectrow].Value.ToString();
                textBox4.Text = dataGridView1[0, selectrow].Value.ToString();
                textBox5.Text = dataGridView1[1, selectrow].Value.ToString();
                textBox6.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox7.Text = dataGridView1[3, selectrow].Value.ToString();

                textBox8.Text = dataGridView1[0, selectrow].Value.ToString();
            }
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox5.Text = "";
            textBox7.Text = "";

            textBox8.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;

            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox1.Text = dataGridView1[4, selectrow].Value.ToString();
                textBox2.Text = dataGridView1[5, selectrow].Value.ToString();
                textBox3.Text = dataGridView1[6, selectrow].Value.ToString();
                textBox4.Text = dataGridView1[0, selectrow].Value.ToString();
                textBox5.Text = dataGridView1[1, selectrow].Value.ToString();
                textBox6.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox7.Text = dataGridView1[3, selectrow].Value.ToString();

                textBox8.Text = dataGridView1[0, selectrow].Value.ToString();
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Необходимо выбрать строку для удаления");
                return;
            }

            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                        "DatA Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;

            sql = " Delete * FROM Родители where id = " + textBox8.Text.ToString();
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "Select * FROM Родители";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox3.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //groupBox3.Visible = false;
        }

     /*   private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox8.Text = "";

            groupBox2.Visible = false;
            groupBox3.Visible = true;
            groupBox4.Visible = false;
            
        
        }
        */
        private void поискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = -1;
            textBox9.Text = "";
            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0)
            {
                MessageBox.Show("Введите критерий поиска");
                return;
            }
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " +
                       "Data Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            string kriteriy = comboBox1.SelectedItem.ToString();
            string poisk = textBox9.Text;
            string sql = "SELECT * FROM Родители WHERE " +
                        kriteriy + " like '" + poisk + "%' ";

            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

        }

        private void button8_Click(object sender, EventArgs e)
        {


            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                       "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;


            string sql = "Select * FROM Родители";

            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
            groupBox4.Visible = false;
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
            groupBox1.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                     "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;

            string sql;

            sql = "INSERT INTO Родители (ФИО_матери, Место_работы_м, Телефон_м, ФИО_отца, Место_работы_от, Телефон_от) " +
                 "VALUES ('" + textBox15.Text + "','" + textBox14.Text + "', '" + textBox13.Text +
                                "','"  + textBox12.Text + "','" + textBox11.Text + "', '" + textBox10.Text + "')";

            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "Select * FROM Родители";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox1.Visible = false;
            groupBox4.Visible = false;
            groupBox3.Visible = false;
            groupBox2.Visible = false;
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox3.Visible = true;
            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox4.Visible = false;

        }
     
       
        
    }
}
