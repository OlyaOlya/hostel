﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace hostel
{
    public partial class StudReport : Form
    {
        public StudReport()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void StudReport_Load(object sender, EventArgs e)
        {
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                       "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;

            string sql = "Select * FROM Студенты ORDER BY Н_комнаты";
            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();

            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "ПІБ");
            ds.WriteXmlSchema("schema.xml");
            connection.Close();

            CrystalReport rpt = new CrystalReport();
            rpt.SetDataSource(ds);
            crystalReportViewer1.ReportSource = rpt;
        }
    }
}
