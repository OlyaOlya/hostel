﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace hostel
{
    public partial class Vigovor : Form
    {
        public int selectrow = -1;

        public Vigovor()
        {
            InitializeComponent();
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = -1;
            comboBox2.Items.Clear();
            if (dataGridView2.RowCount - 1 > 0)
            {
                for (int i = 0; i < dataGridView2.RowCount - 1; i++)
                    comboBox2.Items.Add(dataGridView2[0, i].Value.ToString() + " " + dataGridView2[1, i].Value.ToString());
            }
            dateTimePicker1.Text = "";
            textBox3.Text = "";
            groupBox1.Visible = true;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
        }

        private void редактироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox4.Text = "";
            dateTimePicker2.Text = "";
            comboBox3.Text = "";
            comboBox3.Items.Clear();
            if (dataGridView2.RowCount - 1 > 0)
            {
                for (int i = 0; i < dataGridView2.RowCount - 1; i++)
                    comboBox3.Items.Add(dataGridView2[0, i].Value.ToString() + " " + dataGridView2[1, i].Value.ToString());
            }

            textBox7.Text = "";

            groupBox1.Visible = false;

            groupBox2.Visible = true;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox8.Text = "";


            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = true;
            groupBox4.Visible = false;
        }

        private void поискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = -1;
            textBox9.Text = "";

            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = true;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox4.Text = "";
            dateTimePicker2.Text = "";
            comboBox3.Text = "";
            comboBox3.Items.Clear();
            textBox7.Text = "";

            textBox8.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;

            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox4.Text = dataGridView1[0, selectrow].Value.ToString();
                comboBox3.Text = dataGridView1[1, selectrow].Value.ToString();
                dateTimePicker2.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox7.Text = dataGridView1[3, selectrow].Value.ToString();

                textBox8.Text = dataGridView1[0, selectrow].Value.ToString();

            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox4.Text = "";
            comboBox3.Text = "";
            comboBox3.Items.Clear();
            dateTimePicker2.Text = "";
            textBox7.Text = "";

            textBox8.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;
            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox4.Text = dataGridView1[0, selectrow].Value.ToString();
                comboBox3.Text = dataGridView1[1, selectrow].Value.ToString();
                dateTimePicker2.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox7.Text = dataGridView1[3, selectrow].Value.ToString();

                textBox8.Text = dataGridView1[0, selectrow].Value.ToString();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                      "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;

            string sql;

            sql = "INSERT INTO Выговоры ( id_студента, Дата, Описание )" +
    "VALUES(" + dataGridView2[0, comboBox2.SelectedIndex].Value.ToString() + ",' " + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "', '" + textBox3.Text + "')";

            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT Выговоры.id, Студенты.ФИО, Выговоры.Дата, Выговоры.Описание " +
                "FROM Студенты INNER JOIN Выговоры ON Студенты.[id] = Выговоры.[id_студента]";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox1.Visible = false;
            groupBox4.Visible = false;
            groupBox3.Visible = false;
            groupBox2.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Необходимо выбрать строку для редактирования");
                return;
            }

            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                        "DatA Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;

            sql = " UPDATE Студенты INNER JOIN Выговоры ON Студенты.[id] = Выговоры.[id_студента] SET " +
                "Выговоры.id_студента = " + dataGridView2[0, comboBox3.SelectedIndex].Value.ToString() + "," +
                "Выговоры.Дата = '" + dateTimePicker2.Value.ToString("dd.MM.yyyy") + "'," +
                "Выговоры.Описание = '" + textBox7.Text.ToString() + "'" +
               " WHERE Выговоры.id =" + textBox4.Text.ToString();
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT Выговоры.id, Студенты.ФИО, Выговоры.Дата, Выговоры.Описание " +
                 "FROM Студенты INNER JOIN Выговоры ON Студенты.[id] = Выговоры.[id_студента]";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox1.Visible = false;
            groupBox4.Visible = false;
            groupBox3.Visible = false;
            groupBox2.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Необходимо выбрать строку для удаления");
                return;
            }

            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                        "DatA Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;

            sql = " Delete * FROM Выговоры where id = " + textBox8.Text.ToString();
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT Выговоры.id, Студенты.ФИО, Выговоры.Дата, Выговоры.Описание " +
                "FROM Студенты INNER JOIN Выговоры ON Студенты.[id] = Выговоры.[id_студента]";

            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox3.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0)
            {
                MessageBox.Show("Введите критерий поиска");
                return;
            }
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " +
                       "Data Source=bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            string kriteriy = comboBox1.SelectedItem.ToString();
            string poisk = textBox9.Text;
            string sql = "SELECT Выговоры.id, Студенты.ФИО, Выговоры.Дата, Выговоры.Описание " +
                            "FROM Студенты INNER JOIN Выговоры ON Студенты.[id] = Выговоры.[id_студента]" +
                             " WHERE ";
            switch (comboBox1.SelectedItem.ToString())
            {
                case "ФИО":
                    sql += " Студенты.ФИО" + " like '" + poisk + "%'";
                    break;
                case "Дата":
                    sql += " Выговоры.Дата" + " like '" + poisk + "%' ";
                    break;
                case "Описание":
                    sql += " Выговоры.Описание" + " like '" + poisk + "%'";
                    break;
                case "id":
                    sql += " Выговоры.id = " + poisk + " ";
                    break;

            }

            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();


        }

        private void button8_Click(object sender, EventArgs e)
        {
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                      "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;


            string sql = "SELECT Выговоры.id, Студенты.ФИО, Выговоры.Дата, Выговоры.Описание " +
             "FROM Студенты INNER JOIN Выговоры ON Студенты.[id] = Выговоры.[id_студента]";

            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
            groupBox4.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            groupBox3.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
        }

        private void Vigovor_Load(object sender, EventArgs e)
        {
            String ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                     "Data Source = bd_obsch.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;

            string sql = "SELECT Выговоры.id, Студенты.ФИО, Выговоры.Дата, Выговоры.Описание " +
                "FROM Студенты INNER JOIN Выговоры ON Студенты.[id] = Выговоры.[id_студента]";

            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();

            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Оплата");
            dataGridView1.DataSource = ds.Tables["Оплата"].DefaultView;
            connection.Close();

            connection.ConnectionString = ConnectionString;

            sql = "Select * FROM Студенты";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();

            da = new OleDbDataAdapter(myCommand);
            ds = new DataSet();
            da.Fill(ds, "Студ");
            dataGridView2.DataSource = ds.Tables["Студ"].DefaultView;
            connection.Close();

            /*  for (int i = 0; i < dataGridView2.RowCount - 1; i++)
                  comboBox2.Items.Add(dataGridView2[0, i].Value.ToString() + " " + dataGridView2[1, i].Value.ToString());
              */

            /* if (dataGridView2.RowCount - 1 > 0)
             {
                 for (int i = 0; i < dataGridView2.RowCount - 1; i++)
                     comboBox2.Items.Add(dataGridView2[0, i].Value.ToString() + " " + dataGridView2[1, i].Value.ToString());
             }*/



            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
        }

        private void comboBox3_Click(object sender, EventArgs e)
        {

        }
    }
}
